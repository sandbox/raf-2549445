<?php

/**
 * Cache Tagger configuration form at 'admin/config/cache-tagger/general'.
 */
function cache_tagger_general_form($form, &$form_state) {
  $form['expire_tags'] = array(
    '#type' => 'textarea',
    '#title' => t('Expire tags'),
    '#description' => t("Enter tags which should be expired. One tag per line. Each tag should be a semi-colon concatenated string."),
    '#required' => TRUE,
  );

  $form['actions'] = array(
    '#type' => 'actions',
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Expire'),
  );

  return $form;
}

/**
 * Submit handler for 'cache_tagger_general_form'.
 */
function cache_tagger_general_form_submit($form, $form_state) {
  $expire_tags = empty($form_state['values']['expire_tags']) ? array() : array_map('trim', explode("\n", $form_state['values']['expire_tags']));
  if (!empty($expire_tags)) {
    cache_tagger_expire($expire_tags);
    drupal_set_message(t('The following tags were expired: !tags', array('!tags' => theme('item_list', array('items' => $expire_tags)))));
  }
}

/**
 * Cache Tagger configuration form at 'admin/config/cache-tagger/general/settings'.
 */
function cache_tagger_settings_form($form, &$form_state) {
  // Collect the entity types which may be compatible with Cache Tagger.
  $entity_types = array();
  $entity_info = entity_get_info();
  foreach ($entity_info as $entity_type => $info) {
    if (!empty($info['uri callback'])) {
      $entity_types[$entity_type] = $info['label'];
    }
  }

  $form['cache_tagger_entity_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Entity types'),
    '#description' => t('Select the entity types for which Cache Tagger should tag pages.'),
    '#options' => $entity_types,
    '#default_value' => variable_get('cache_tagger_entity_types'),
  );

  return system_settings_form($form);
}

/**
 * Validation for 'cache_tagger_settings_form'.
 */
function cache_tagger_settings_form_validate($form, &$form_state) {
  $form_state['values']['cache_tagger_entity_types'] = array_values(array_filter($form_state['values']['cache_tagger_entity_types'], 'cache_tagger_not_empty'));
}
