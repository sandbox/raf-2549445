<?php

/**
 * Act upon registering tags for the current request.
 *
 * @param array $tags
 *   One or more tags. Each tag is a semi-colon concatenated string.
 */
function hook_cache_tagger_request($tags) {
  // For an example, check out the Cache Tagger Tracker implementation for
  // this hook: cache_tagger_tracker_cache_tagger_request().
}

/**
 * Act upon the expiration of tags.
 *
 * @param array $tags
 *   One or more tags. Each tag is a semi-colon concatenated string.
 */
function hook_cache_tagger_expire($tags) {
  // For an example, check out the Cache Tagger Varnish implementation for
  // this hook: cache_tagger_varnish_cache_tagger_expire().
}

/**
 * Alter entity expire tags.
 *
 * @param array $expire_tags
 *   One or more tags. Each tag is a semi-colon concatenated string.
 * @param array $context
 *   Associative array with the following key/value pairs:
 *     - entity_type: the entity type
 *     - entity: the entity object
 */
function hook_cache_tagger_entity_expire_tags_alter(&$expire_tags, $context) {
  // Prevent changes to unpublished nodes incurring cache expirations.
  if ($context['entity_type'] === 'node' && $context['entity']->status !== NODE_PUBLISHED) {
    $expire_tags = array();
  }
}

/**
 * Alter path expire tags.
 *
 * @param array $expire_tags
 *   One or more tags. Each tag is a semi-colon concatenated string.
 * @param array $context
 *   Associative array as provided by the path module in the hook_path_insert(),
 *   hook_path_update() and hook_path_delete() hooks.
 */
function hook_cache_tagger_path_expire_tags_alter(&$expire_tags, $context) {
  // Don't expire by path if the alias hasn't changed.
  if (!empty($context['path']['alias']) && !empty($context['path']['original']['alias']) && $context['path']['alias'] === $context['path']['original']['alias']) {
    $expire_tags = array();
  }
}
