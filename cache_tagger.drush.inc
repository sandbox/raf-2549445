<?php

/**
 * Implements hook_drush_command().
 */
function cache_tagger_drush_command() {
  $items = array();

  $items['cache-tagger-expire'] = array(
    'description' => "Expire tags.",
    'arguments' => array(
      'tags' => "One or more tags to expire.",
    ),
    'examples' => array(
      'drush ct-expire page:front' => "Expire the front page.",
      'drush ct-expire one:tag another:tag and:onemore' => "Expire multiple tags at once.",
    ),
    'aliases' => array('ct-expire'),
  );

  return $items;
}

/**
 * Prime the cache by crawling a batch of expired URL's.
 */
function drush_cache_tagger_expire() {
  $tags = func_get_args();
  if (empty($tags)) {
    return drush_set_error(dt('Please give one or more tags to expire.'));
  }
  else {
    cache_tagger_expire(array_map('trim', $tags));
    drush_log(dt('The following tags were expired: @tags.', array('@tags' => implode(', ', $tags))), 'success');
  }
}
