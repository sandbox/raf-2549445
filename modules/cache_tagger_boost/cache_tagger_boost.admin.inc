<?php

/**
 * Cache Tagger configuration form at 'admin/config/cache-tagger/boost'.
 */
function cache_tagger_boost_settings_form($form, &$form_state) {
  $form['cache_tagger_boost_expire_batch_process_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Expire batch process limit'),
    '#description' => t('Maximum amount of Boost files which can be deleted at once.'),
    '#default_value' => variable_get('cache_tagger_boost_expire_batch_process_limit', CACHE_TAGGER_BOOST_DEFAULT_EXPIRE_BATCH_PROCESS_LIMIT),
    '#size' => 10,
  );

  return system_settings_form($form);
}
