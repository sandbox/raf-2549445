<?php

/**
 * Implements hook_drush_command().
 */
function cache_tagger_boost_drush_command() {
  $items = array();

  $items['cache-tagger-boost-clean'] = array(
    'description' => "Checks and and cleans up untracked Boost entries. Untracked Boost entries need to be prevented at all times, since they become stale and will never get a refresh.",
    'aliases' => array('ctb-clean'),
  );

  return $items;
}

/**
 * Cleans up untracked Boost entries.
 */
function drush_cache_tagger_boost_clean() {
  // Retrieve all the domain directories in the Boost cache directory.
  $boost_dir = boost_get_normal_cache_dir();
  $domain_directories = glob($boost_dir . '/*' , GLOB_ONLYDIR);  
  // Process each domain directory individually.
  $count = 0;
  foreach ($domain_directories as $domain_directory) {
    // Construct the domain name.
    $domain = substr($domain_directory, strrpos($domain_directory, '/') + 1);  
    // Retrieve all the files in this domain directory.
    $domain_files = file_scan_directory($domain_directory , '/\./', array(), 1);
    // Log the domain we are about to process.
    drush_log(dt('Processing the "@domain" domain (@count files found)...', array('@domain' => $domain, '@count' => count($domain_files))), 'warning');
    // Only continue processing this domain if there are any files.
    $domain_count = 0;
    if (!empty($domain_files)) {
      // Construct a (protocol relative) URL for each file.
      $domain_files_urls = array_combine(array_keys($domain_files), array_keys($domain_files));
      $domain_files_urls = str_replace(array(boost_get_normal_cache_dir() . '/', variable_get('boost_char', BOOST_CHAR), '?.'), array('//', '?', '.'), $domain_files_urls);
      foreach ($domain_files_urls as $key => $url) {
        $domain_files_urls[$key] = substr($url, 0, strrpos($url, '.'));
      }
      // Check each URL.
      foreach ($domain_files_urls as $file => $url) {
        $result = cache_tagger_tracker_urls(1, $url);
        // If the URL is not tracked, the Boost entry is deleted.
        if (empty($result)) {
          drush_log(dt('Untracked URL "@url" removed from Boost.', array('@url' => $url)), 'warning');
          unlink($file);
          ++$domain_count;
        }
      }
    }
    // When current domain has finished processing, display a short domain summary.
    drush_log(format_plural($domain_count, '1 untracked URL was found and removed from Boost for the "@domain" domain.', '@count untracked URL\'s were found and removed from Boost for the "@domain" domain.', array('@domain' => $domain)), 'warning');
    // Add the domain count to the global count.
    $count += $domain_count;
  }
  // When all domains have been processed, display a short global summary.
  drush_log(format_plural($count, 'Finished: 1 untracked URL was found and removed from Boost.', 'Finished: @count untracked URL\'s were found and removed from Boost.', array('@domain' => $domain)), 'ok');  
}
