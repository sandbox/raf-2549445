<?php

/**
 * Cache Tagger configuration form at 'admin/config/cache-tagger/crawler'.
 */
function cache_tagger_crawler_build_form($form, &$form_state) {
  if (module_exists('background_batch')) {
    $form['background'] = array(
      '#type' => 'checkbox',
      '#title' => t('Background batch'),
      '#description' => t('Perform the build in a background batch.'),
      '#default_value' => TRUE,
    );
  }

  $types = cache_tagger_crawler_build_types();
  $form['types'] = array(
    '#type' => 'checkboxes',
    '#title' =>  t('Types'),
    '#description' => t('Type of items the tracker should be built for.'),
    '#options' => $types,
    '#required' => TRUE,
  );

  $form['actions'] = array(
    '#type' => 'actions',
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Build'),
  );

  return $form;
}

/**
 * Validation for 'cache_tagger_crawler_build_form'.
 */
function cache_tagger_crawler_build_form_validate($form, &$form_state) {
  $form_state['values']['types'] = array_values(array_filter($form_state['values']['types'], 'cache_tagger_not_empty'));
}

/**
 * Submit handler for 'cache_tagger_crawler_build_form'.
 */
function cache_tagger_crawler_build_form_submit($form, &$form_state) {
  $batch = cache_tagger_crawler_create_build_batch($form_state['values']['types']);
  batch_set($batch);
  if (!empty($form_state['values']['background'])) {
    background_batch_process_batch();
    drupal_set_message(t('Tracker is being built in the background.'));
  }
}

/**
 * Cache Tagger configuration form at
 * 'admin/config/cache-tagger/crawler/settings'.
 */
function cache_tagger_crawler_settings_form($form, &$form_state) {
  $form['crawling'] = array(
    '#type' => 'fieldset',
    '#title' => t('Crawling'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['crawling']['cache_tagger_crawler_crawl_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Crawl limit'),
    '#description' => t("Maximum amount of URL's that may be crawled at once."),
    '#default_value' => variable_get('cache_tagger_crawler_crawl_limit', CACHE_TAGGER_CRAWLER_DEFAULT_CRAWL_LIMIT),
  );

  $form['crawling']['cache_tagger_crawler_priority_tags'] = array(
    '#type' => 'textarea',
    '#title' => t('Priority tags'),
    '#description' => t("URL's which are tagged with priority tags will be crawled immediately after expiring. One tag per line. Each tag should be a semi-colon concatenated string."),
    '#default_value' => implode("\n", cache_tagger_crawler_priority_tags()),
  );

  $form['building'] = array(
    '#type' => 'fieldset',
    '#title' => t('Building'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  // Collect the Cache Tagger compatible entity types.
  $entity_types = array();
  $valid_entity_types = cache_tagger_entity_types();
  $entity_info = entity_get_info();
  foreach ($valid_entity_types as $entity_type) {
    $entity_types[$entity_type] = $entity_info[$entity_type]['label'];
  }

  $form['building']['cache_tagger_crawler_entity_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Entity types'),
    '#description' => t('Select the entity types which may be processed when building the tracker.'),
    '#options' => $entity_types,
    '#default_value' => variable_get('cache_tagger_crawler_entity_types'),
  );

  $form['building']['cache_tagger_crawler_entity_batch_process_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Entity batch process limit'),
    '#description' => t('Maximum amount of entities that may be processed at once while building the tracker.'),
    '#default_value' => variable_get('cache_tagger_crawler_entity_batch_process_limit', CACHE_TAGGER_CRAWLER_DEFAULT_ENTITY_BATCH_PROCESS_LIMIT),
  );

  return system_settings_form($form);
}

/**
 * Validation for 'cache_tagger_crawler_settings_form'.
 */
function cache_tagger_crawler_settings_form_validate($form, &$form_state) {
  $form_state['values']['cache_tagger_crawler_entity_types'] = array_values(array_filter($form_state['values']['cache_tagger_crawler_entity_types'], 'cache_tagger_not_empty'));
  $form_state['values']['cache_tagger_crawler_priority_tags'] = empty($form_state['values']['cache_tagger_crawler_priority_tags']) ? array() : array_map('trim', explode("\n", $form_state['values']['cache_tagger_crawler_priority_tags']));
}
