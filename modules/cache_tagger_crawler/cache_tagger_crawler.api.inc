<?php

/**
 * Modify the options used by HTTPRL when sending crawling a URL.
 *
 * @param array $options
 *   See $options for httprl_request().
 * @param array $context
 *   Array with contextual information:
 *     - 'url': URL the options can be altered for.
 */
function hook_cache_tagger_crawler_url_options(&$options, $context) {
  // Make all requests blocking.
  $options['blocking'] = TRUE;
}

/**
 * Modify the batch operations used to build the tracker.
 *
 * @param array $operations
 *   Operations array as used in the Batch API.
 * @param array $context
 *   Array with contextual information for this batch:
 *     - 'types': array with types of pages (eg. entity, view, ...) to build
 *       the tracker with. If empty, all types are assumed.
 */
function hook_cache_tagger_crawler_build_batch_operations_alter(&$operations, $context) {
  // We have a few extra custom pages we want to be added to the tracker
  // when it's being built. Our custom operation will eventually save each URL
  // to the tracker using cache_tagger_tracker_save().
  if (empty($context['types']) || in_array('custom', $context['types'])) {
    $operations[] = array('my_custom_pages_batch_worker', array());
  }
}

/**
 * Modify the URL's for an entity when building the tracker.
 *
 * @param array $urls
 *   URL's derived from this entity. May be zero, one or multiple URL's.
 * @param type $context
 *   Array containing contextual information:
 *     - 'entity_uri': entity URI as returned by entity_uri()
 *     - 'entity_type': the entity's type
 *     - 'entity': the entity object
 *     - 'original_url': the original generated URL
 */
function hook_cache_tagger_crawler_entity_urls_alter(&$urls, $context) {
  // If a node, we want it to be published in order to be added to the tracker.
  if ($context['entity_type'] === 'node' && $context['entity']->status !== NODE_PUBLISHED) {
    $urls = array();
  }
}

/**
 * Modify the URL's for a View when building the tracker.
 *
 * @param array $urls
 *   URL's derived from this View. May be zero, one or multiple URL's.
 * @param type $context
 *   Array containing contextual information:
 *     - 'view': the view object
 *     - 'display': the view's display
 *     - 'original_url': the original generated URL
 */
function hook_cache_tagger_crawler_views_urls_alter(&$urls, $context) {
  // If a view, we need it to have a specific tag to be added to the tracker.
  if ($context['view']->tag !== 'my_tag') {
    $urls = array();
  }
}

/**
 * Add or modify types of items the tracker can be built for.
 *
 * @param array $types
 *   Associative array, keyed by type and it's label as value.
 */
function hook_cache_tagger_crawler_build_types_alter(&$types) {
  $types['my_custom_item'] = t('My custom item');
}
