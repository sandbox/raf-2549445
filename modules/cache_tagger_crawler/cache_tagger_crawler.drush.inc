<?php

/**
 * Implements hook_drush_command().
 */
function cache_tagger_crawler_drush_command() {
  $items = array();

  $items['cache-tagger-crawler-crawl'] = array(
    'description' => "Primes the cache by crawling a batch of expired URL's.",
    'options' => array(
      'limit' => "Maximum amount of URL's to crawl.",
      'url_filter' => "Comma-separated strings to filter URL's by. Use the % character for wildcard matching.",
      'tag_filter' => "Comma-separated cache tags to filter by. Each tag is a semi-colon concatenated string",
      'url_filter_condition' => "The condition used for url filtering (OR or AND). By default, the OR-condition is assumed.",
      'tag_filter_condition' => "The condition used for tag filtering (OR or AND). By default, the OR-condition is assumed.",
    ),
    'examples' => array(
      'drush ctc-crawl --limit=20' => "Crawls 20 expired URL's.",
    ),
    'aliases' => array('ctc-crawl'),
  );

  $items['cache-tagger-crawler-build'] = array(
    'description' => "Builds the tracker by adding missing URL's.",
    'options' => array(
      'types' => "Comma-delimited string of URL types (entity, view, ...)",
    ),
    'examples' => array(
      'drush ctc-build --types=entity' => "Builds the tracker by adding missing entity URL's.",
    ),
    'aliases' => array('ctc-build'),
  );

  return $items;
}

/**
 * Prime the cache by crawling a batch of expired URL's.
 */
function drush_cache_tagger_crawler_crawl() {
  // Get the options.
  $limit = drush_get_option('limit');
  $url_filter = drush_get_option('url_filter', NULL);
  $tag_filter = drush_get_option('tag_filter', NULL);
  $url_filter_condition = drush_get_option('url_filter_condition');
  $tag_filter_condition = drush_get_option('tag_filter_condition');
  // Preprocess input.
  $limit = variable_get('cache_tagger_crawler_crawl_limit', CACHE_TAGGER_CRAWLER_DEFAULT_CRAWL_LIMIT);
  $url_filter = !empty($url_filter) ? explode(',', $url_filter) : array();
  $tag_filter = !empty($tag_filter) ? explode(',', $tag_filter) : array();
  // Get the URL's to crawl.
  $urls = cache_tagger_tracker_urls($limit, $url_filter, $tag_filter, TRUE, NULL, $url_filter_condition, $tag_filter_condition);
  // Crawl the URL's, if any.
  if (!empty($urls)) {
    cache_tagger_crawler_crawl($urls, $limit);
  }

  drush_log(count($urls) . " expired URL's were crawled.", 'success');
}

/**
 * Builds the tracker by adding missing URL's.
 */
function drush_cache_tagger_crawler_build() {
  $types = drush_get_option('types');
  $types = empty($types) ? array() : explode(',', $types);
  $batch = cache_tagger_crawler_create_build_batch($types);
  batch_set($batch);
  $batch =& batch_get();
  $batch['progressive'] = FALSE;
  drush_backend_batch_process();
}
