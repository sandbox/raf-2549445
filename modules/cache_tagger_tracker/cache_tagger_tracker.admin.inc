<?php

/**
 * Cache Tagger configuration form at 'admin/config/cache-tagger/tracker'.
 */
function cache_tagger_tracker_settings_form($form, &$form_state) {
  $form['cache_tagger_tracker_url_scheme'] = array(
    '#type' => 'textfield',
    '#title' => t('URL scheme'),
    '#description' => t("Internally, the tracker works with protocol-relative URL's. Configure the URL scheme to be used with URL's when the tracker is queried."),
    '#default_value' => cache_tagger_tracker_url_scheme(),
    '#size' => 8
  );

  return system_settings_form($form);
}
