<?php

/**
 * Alter a desanitized URL.
 *
 * @param array $parsed_url
 *   The return value of parse_url().
 * @param array $context
 *   Associative array with the following key/value pairs:
 *     - original_url: the original URL
 */
function hook_cache_tagger_tracker_desanitize_url_alter(&$parsed_url, $context) {
  // For some reason, all URL's containing the 'secure' word, are served
  // through https.
  if ($parsed_url['scheme'] !== 'https' && strpos($context['original_url'], 'secure') !== FALSE) {
    $parsed_url['scheme'] = 'https';
  }
}
