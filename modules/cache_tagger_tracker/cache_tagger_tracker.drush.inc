<?php

/**
 * Implements hook_drush_command().
 */
function cache_tagger_tracker_drush_command() {
  $items = array();

  $items['cache-tagger-tracker-clear'] = array(
    'description' => 'Clears all entries from the tracker table.',
    'aliases' => array('ctt-clear'),
  );

  $items['cache-tagger-tracker-expire'] = array(
    'description' => "Mark items as expired in the tracker table. Please note that this does not expire any cache, but updates the tracker table.",
    'options' => array(
      'url_filter' => "Comma-separated strings to filter URL's by. Use the % character for wildcard matching.",
      'tag_filter' => "Comma-separated cache tags to filter by. Each tag is a semi-colon concatenated string",
      'url_filter_condition' => "The condition used for url filtering (OR or AND). By default, the OR-condition is assumed.",
      'tag_filter_condition' => "The condition used for tag filtering (OR or AND). By default, the OR-condition is assumed.",
    ),
    'examples' => array(
      'drush ctt-expire --tag_filter=page:front' => "Marks the front page(s) as expired.",
    ),
    'aliases' => array('ctt-expire'),
  );

  return $items;
}

/**
 * Clears all entries from the tracker table.
 */
function drush_cache_tagger_tracker_clear() {
  cache_tagger_tracker_delete(FALSE);
  drush_log('The tracker table was cleared.', 'success');
}

/**
 * Marks all entries from the tracker table as expired.
 */
function drush_cache_tagger_tracker_expire() {
  // Get the options.
  $url_filter = drush_get_option('url_filter', NULL);
  $tag_filter = drush_get_option('tag_filter', NULL);
  $url_filter_condition = drush_get_option('url_filter_condition');
  $tag_filter_condition = drush_get_option('tag_filter_condition');
  // Preprocess input.
  $url_filter = !empty($url_filter) ? explode(',', $url_filter) : array();
  $tag_filter = !empty($tag_filter) ? explode(',', $tag_filter) : array();

  // Expire the entries.
  $count = cache_tagger_tracker_expire_urls($url_filter, $tag_filter, NULL, $url_filter_condition, $tag_filter_condition);

  drush_log(count($count) . " entries were expired.", 'success');
}
