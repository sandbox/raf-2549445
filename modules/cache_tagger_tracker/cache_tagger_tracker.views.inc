<?php

/**
 * Implements hook_views_data().
 */
function cache_tagger_tracker_views_data() {
  $data = array();

  $data['cache_tagger_tracker']['table']['group'] = t('Cache Tagger: Tracker');

  $data['cache_tagger_tracker']['table']['base'] = array(
    'field' => 'url',
    'title' => t('Cache Tagger: Tracker'),
    'help' => t('Tracks pages along with their cache tags and cache status.'),
  );

  $data['cache_tagger_tracker']['url'] = array(
    'title' => t('URL'),
    'help' => t('URL of the tracked page.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['cache_tagger_tracker']['tags'] = array(
    'title' => t('Tags'),
    'help' => t('Cache tags associated with the tracked page.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['cache_tagger_tracker']['expired'] = array(
    'title' => t('Expired'),
    'help' => t('Indication wether the tracked page has been expired (1) or is valid (0).'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );

  $data['cache_tagger_tracker']['fetched'] = array(
    'title' => t('Fetched'),
    'help' => t('A date indicating when this page was last fetched (non-cached).'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_post_execute().
 */
function cache_tagger_tracker_views_post_execute(&$view) {
  // Desanitize the URL's.
  if ($view->base_table === 'cache_tagger_tracker') {
    foreach ($view->result as $result) {
      if (!empty($result->url)) {
        $result->url = cache_tagger_tracker_desanitize_url($result->url);
      }
    }
  }
}

/**
 * Implements hook_preprocess_views_view_field().
 */
function cache_tagger_tracker_preprocess_views_view_field(&$variables) {
  // Render the tags comma-seperated.
  if ($variables['field']->field_alias === 'cache_tagger_tracker_tags' && !empty($variables['output'])) {
    $variables['output'] = ltrim($variables['output'], '|');
    $variables['output'] = rtrim($variables['output'], '|');
    $variables['output'] = str_replace('|', ', ', $variables['output']);
  }
}
