<?php

/**
 * Implements hook_views_default_views().
 */
function cache_tagger_tracker_views_default_views() {
  $view = new view();
  $view->name = 'cache_tagger_tracker';
  $view->description = 'An overview of tracked pages along with their cache tags and cache status.';
  $view->tag = 'cache_tagger';
  $view->base_table = 'cache_tagger_tracker';
  $view->human_name = 'Cache Tagger: Tracker';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Overview';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer cache_tagger';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Submit';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Reset';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Sort on';
  $handler->display->display_options['exposed_form']['options']['sort_asc_label'] = 'Ascending';
  $handler->display->display_options['exposed_form']['options']['sort_desc_label'] = 'Descending';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'url' => 'url',
    'tags' => 'tags',
    'expired' => 'expired',
    'fetched' => 'fetched',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'url' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'tags' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'expired' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'fetched' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Footer: Global: Result summary */
  $handler->display->display_options['footer']['result']['id'] = 'result';
  $handler->display->display_options['footer']['result']['table'] = 'views';
  $handler->display->display_options['footer']['result']['field'] = 'result';
  /* Field: Cache Tagger: Tracker: URL */
  $handler->display->display_options['fields']['url']['id'] = 'url';
  $handler->display->display_options['fields']['url']['table'] = 'cache_tagger_tracker';
  $handler->display->display_options['fields']['url']['field'] = 'url';
  $handler->display->display_options['fields']['url']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['url']['alter']['path'] = '[url]';
  $handler->display->display_options['fields']['url']['alter']['external'] = TRUE;
  $handler->display->display_options['fields']['url']['alter']['max_length'] = '115';
  $handler->display->display_options['fields']['url']['alter']['trim'] = TRUE;
  /* Field: Cache Tagger: Tracker: Tags */
  $handler->display->display_options['fields']['tags']['id'] = 'tags';
  $handler->display->display_options['fields']['tags']['table'] = 'cache_tagger_tracker';
  $handler->display->display_options['fields']['tags']['field'] = 'tags';
  $handler->display->display_options['fields']['tags']['label'] = 'Labels';
  /* Field: Cache Tagger: Tracker: Expired */
  $handler->display->display_options['fields']['expired']['id'] = 'expired';
  $handler->display->display_options['fields']['expired']['table'] = 'cache_tagger_tracker';
  $handler->display->display_options['fields']['expired']['field'] = 'expired';
  $handler->display->display_options['fields']['expired']['not'] = 0;
  /* Field: Cache Tagger: Tracker: Fetched */
  $handler->display->display_options['fields']['fetched']['id'] = 'fetched';
  $handler->display->display_options['fields']['fetched']['table'] = 'cache_tagger_tracker';
  $handler->display->display_options['fields']['fetched']['field'] = 'fetched';
  $handler->display->display_options['fields']['fetched']['date_format'] = 'short';
  $handler->display->display_options['fields']['fetched']['second_date_format'] = 'long';
  /* Filter criterion: Cache Tagger: Tracker: URL */
  $handler->display->display_options['filters']['url']['id'] = 'url';
  $handler->display->display_options['filters']['url']['table'] = 'cache_tagger_tracker';
  $handler->display->display_options['filters']['url']['field'] = 'url';
  $handler->display->display_options['filters']['url']['operator'] = 'contains';
  $handler->display->display_options['filters']['url']['group'] = 1;
  $handler->display->display_options['filters']['url']['exposed'] = TRUE;
  $handler->display->display_options['filters']['url']['expose']['operator_id'] = 'url_op';
  $handler->display->display_options['filters']['url']['expose']['label'] = 'URL';
  $handler->display->display_options['filters']['url']['expose']['operator'] = 'url_op';
  $handler->display->display_options['filters']['url']['expose']['identifier'] = 'url';
  $handler->display->display_options['filters']['url']['expose']['remember_roles'] = array();
  $handler->display->display_options['filters']['url']['expose']['autocomplete_items'] = '10';
  $handler->display->display_options['filters']['url']['expose']['autocomplete_field'] = 'url';
  $handler->display->display_options['filters']['url']['expose']['autocomplete_raw_suggestion'] = 1;
  $handler->display->display_options['filters']['url']['expose']['autocomplete_raw_dropdown'] = 1;
  $handler->display->display_options['filters']['url']['expose']['autocomplete_dependent'] = 0;
  /* Filter criterion: Cache Tagger: Tracker: Tags */
  $handler->display->display_options['filters']['tags']['id'] = 'tags';
  $handler->display->display_options['filters']['tags']['table'] = 'cache_tagger_tracker';
  $handler->display->display_options['filters']['tags']['field'] = 'tags';
  $handler->display->display_options['filters']['tags']['operator'] = 'contains';
  $handler->display->display_options['filters']['tags']['group'] = 1;
  $handler->display->display_options['filters']['tags']['exposed'] = TRUE;
  $handler->display->display_options['filters']['tags']['expose']['operator_id'] = 'tags_op';
  $handler->display->display_options['filters']['tags']['expose']['label'] = 'Labels';
  $handler->display->display_options['filters']['tags']['expose']['operator'] = 'tags_op';
  $handler->display->display_options['filters']['tags']['expose']['identifier'] = 'tags';
  $handler->display->display_options['filters']['tags']['expose']['remember_roles'] = array();
  $handler->display->display_options['filters']['tags']['expose']['autocomplete_items'] = '10';
  $handler->display->display_options['filters']['tags']['expose']['autocomplete_field'] = 'tags';
  $handler->display->display_options['filters']['tags']['expose']['autocomplete_raw_suggestion'] = 1;
  $handler->display->display_options['filters']['tags']['expose']['autocomplete_raw_dropdown'] = 1;
  $handler->display->display_options['filters']['tags']['expose']['autocomplete_dependent'] = 0;
  /* Filter criterion: Cache Tagger: Tracker: Expired */
  $handler->display->display_options['filters']['expired']['id'] = 'expired';
  $handler->display->display_options['filters']['expired']['table'] = 'cache_tagger_tracker';
  $handler->display->display_options['filters']['expired']['field'] = 'expired';
  $handler->display->display_options['filters']['expired']['group'] = 1;
  $handler->display->display_options['filters']['expired']['exposed'] = TRUE;
  $handler->display->display_options['filters']['expired']['expose']['operator_id'] = '';
  $handler->display->display_options['filters']['expired']['expose']['label'] = 'Expired';
  $handler->display->display_options['filters']['expired']['expose']['operator'] = 'expired_op';
  $handler->display->display_options['filters']['expired']['expose']['identifier'] = 'expired';
  $handler->display->display_options['filters']['expired']['expose']['remember_roles'] = array();
  /* Filter criterion: Cache Tagger: Tracker: Fetched */
  $handler->display->display_options['filters']['fetched']['id'] = 'fetched';
  $handler->display->display_options['filters']['fetched']['table'] = 'cache_tagger_tracker';
  $handler->display->display_options['filters']['fetched']['field'] = 'fetched';
  $handler->display->display_options['filters']['fetched']['group'] = 1;
  $handler->display->display_options['filters']['fetched']['exposed'] = TRUE;
  $handler->display->display_options['filters']['fetched']['expose']['operator_id'] = 'fetched_op';
  $handler->display->display_options['filters']['fetched']['expose']['label'] = 'Fetched';
  $handler->display->display_options['filters']['fetched']['expose']['operator'] = 'fetched_op';
  $handler->display->display_options['filters']['fetched']['expose']['identifier'] = 'fetched';
  $handler->display->display_options['filters']['fetched']['expose']['remember_roles'] = array();

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/config/cache-tagger/tracker/overview';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Overview';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $handler->display->display_options['tab_options']['title'] = 'Overview';
  $handler->display->display_options['tab_options']['weight'] = '0';
  $translatables['cache_tagger_tracker'] = array(
    t('Master'),
    t('Overview'),
    t('more'),
    t('Submit'),
    t('Reset'),
    t('Sort on'),
    t('Ascending'),
    t('Descending'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('Displaying @start - @end of @total'),
    t('URL'),
    t('Labels'),
    t('Expired'),
    t('Fetched'),
    t('Page'),
  );

  return array($view->name => $view);
}
