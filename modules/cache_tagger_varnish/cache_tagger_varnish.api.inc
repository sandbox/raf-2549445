<?php

/**
 * Alter Cache Tagger commands sent to Varnish.
 *
 * @param array $commands
 *   Zero, one or multiple Varnish ban or purge commands. Each command is a
 *   string.
 * @param type $context
 *   Array containing contextual information:
 *     - 'version': the Varnish version in use
 *     - 'command': the command (ban or purge)
 *     - 'host': the host used in the commands, as returned by
 *        _varnish_get_host()
 *     - 'tags': array of tags used to create the commands. Each tag is a
 *        semi-colon concatenated string.
 */
function hook_cache_tagger_varnish_expire_commands_alter(&$commands, $context) {
  // Drupal installation is reachable through 2 different hosts. Make sure
  // the tags are expired for both hosts.
  $extra_host = strpos($context['host'], 'ahost.com') === 0 ? 'anotherhost.com' : 'ahost.com';
  $extra_commands = array();
  foreach ($commands as $command) {
    $extra_commands[] = str_replace($context['host'], $extra_host, $command);
  }
  $commands = array_merge($commands, $extra_commands);
}
